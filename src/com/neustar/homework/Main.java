package com.neustar.homework;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * @author Deuvarney Sanderson
 * 
 * The main purpose of this class is to process Categories and subcategories 
 * from a file on the host system and outputs the number of subcategories
 * associated with valid category and the processed lines of the input file in 
 * order of when it came in. Other functions have been made available publicly to ease testing
 * and allow users to use various functionality of this code that best suits their needs.
 *
 */
/**
 * @author davon
 *
 */
public class Main {

	// Set List for acceptable category names.
	// Using an array list is has faster readers compared to other lists(such as LinkedList)
	public final static List<String> acceptedCategories = new ArrayList<String>(
			Arrays.asList("PERSON", "PLACE", "ANIMAL", "COMPUTER", "OTHER"));
	// Set category size in order to not query the size of a immutable list
	// Allows the program to cut down on calling acceptedCategories.size() multiple times throughout the program
	public final static int acceptedCategoriesSize = acceptedCategories.size();
	// Retain size and sub-categories of each category
	// Used to easily get/set the values/size of the lines that are being processed based on category name
	private static Map<String, List<String>> savedCategoriesMap = new HashMap<String, List<String>>();
	// Retain order of incoming (Sub)Categories
	// This ArrayList should have the same write speed (in comparison to a LinkedList) when appending to the end of the list
	// Uses less memory resources as compared to a LinkedLists
	private static List<String> savedCategoriesList = new ArrayList<String>();

	/**
	 * 
	 * @param args Expects the first value of the array to be a filepath to the input (Sub)Category file. 
	 * Will not operate on any subsequent parameters.
	 * Will output the number of subcategories associated with valid category as well as the order
	 * of the processed lines of (sub)categories as they were read in
	 * 
	 */
	public static void main(String args[]) {
		//Checks if an argument was supplied and exits with Status 1 if not supplied
		if (args.length < 1) {
			//Used to simulate logging factory that would usually be implemented in much bigger applications
			System.out.print("ERROR: Required file directory was not supplied");
			System.exit(1);
		//Checks if the length of the supplied arguments is greater than one and alerts the user to the actions that the program will take
		} else if (args.length > 1) {
			System.out.print("WARN: Multiple parameters detected. Only using the first as the required file path");
		}
		
		//Read files in as a stream (using newer/more efficient Java 8 methods)
		try (Stream<String> stream = Files.lines(Paths.get(args[0]))) {
			//Iterate over the streamed input file
			for (Object line : stream.toArray()) {
				//Set category and subCategory string once to avoid multiple calls to respective methods
				String category = getCategory((String) line);
				String subCategory = getSubCategory((String) line);
				//Determine whether the provided line is valid and that it does not already exist in the saved list & map
				if (determineValidLine( category,  subCategory, acceptedCategories, acceptedCategoriesSize)
						&& !determineIfSubCategoryExistsInSavedCategories(category, subCategory, savedCategoriesMap)) {
					//Add values to the saved list/map to be provided to the user in the console
					addLineToSavedMap(category, subCategory, savedCategoriesMap);
					addLineToSavedList((String) line, savedCategoriesList);
				}
			}
			//Display required information
			displayCategoryCount(savedCategoriesMap);
			displayOrderedCategories(savedCategoriesList);
		}
		//Exit with Status 2 if the file provided in the argument does not exist
			catch (NoSuchFileException e) {
			System.out.print("ERROR: Supplied file path does not exist");
			System.exit(2);
		//Exit with Status 3 for possible  IO exception when reading the file as a stream	
		} catch (IOException e ){
				System.out.print("ERROR: IO Exception occured");
				e.printStackTrace();
				System.exit(3);
			}
	}
	
	//
	/**
	 * This method will output the count associated with each category in @param categoryMapping
	 * @param categoryMapping - Accepts a Map<String, List<String>> reference. This  value will come from this.savedCategoriesMap
	 */
	private static void displayCategoryCount(Map<String, List<String>> categoryMapping) {
		System.out.println("CATEGORY\t\tCOUNT");
		for (String category : acceptedCategories) {
			List<String> subCategories = categoryMapping.get(category);
			System.out
					.println(String.format("%s\t\t\t%d", category, (subCategories == null) ? 0 : subCategories.size()));
		}

	}

	/**
	 * This method will output the processed lines from the input file in the order that they were read in
	 * @param categoriesList - Accepts a List<String> reference. This value  will come from this.savedCategoriesList
	 * 
	 */
	private static void displayOrderedCategories(List<String> categoriesList) {
		for (String line : categoriesList) {
			System.out.println(line);
		}
	}
	
	/**
	 * @param line - Line that you want to have inserted into list. (Default implementation takes this from the input file)
	 * @param list - List that you want to have line added to.(Default implementation adds to this.savedCategoriesList)
	 */
	public static void addLineToSavedList(String line, List<String> list) {
		list.add(line);
	}

	/**
	 * @param category
	 * @param subCategory
	 * @param map
	 */
	public static void addLineToSavedMap(String category, String subCategory, Map<String, List<String>> map) {

		List<String> subCategoryList = getSubCategoryListInSavedCategories(category, map);
		if (subCategoryList != null) {
			subCategoryList.add(subCategory);
		} else {
			subCategoryList = new ArrayList<String>();
			subCategoryList.add(subCategory);

		}
		map.put(category, subCategoryList);
	}

	/**
	 * @param category
	 * @param subCategory
	 * @param map - Paramter to be tested for 
	 * @return - Boolean determining if the subCategory exists for the provided category and map
	 */
	public static boolean determineIfSubCategoryExistsInSavedCategories(String category, String subCategory, Map<String, List<String>> map) {
		List<String> categoryList = getSubCategoryListInSavedCategories(category, map);
		if (categoryList != null) {
			for (String subCategoryFromList : categoryList) {
				if (subCategoryFromList.equals(subCategory)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Function used for retrieving the @param category associated with @param map
	 * 
	 * @param category - Category to search @param map
	 * @param map
	 * @return - List<String> for the value associated with @param category in @param map
	 */
	public static List<String> getSubCategoryListInSavedCategories(String category, Map<String, List<String>> map) {
		return map.get(category);
	}

	/**
	 * Tests to see if the provided category and subCategory and valid for the provided list
	 * 
	 * @param category - Category to be used for validation
	 * @param subCategory - Sub category to be used for validation
	 * @param list - List that contains all valid categories
	 * @param listSize - Size of the @param list
	 * @return - Boolean determining the validity of category and subcategory
	 */
	public static boolean determineValidLine(String category, String subCategory, List<String> list, int listSize) {

		return determineValidCategory(category, list, listSize) && determineValidSubCategory(subCategory);
	}

	/**
	 * @param category - Category to be added to @param list
	 * @param list - List that contains all valid categories
	 * @param listSize -  Size of the @param list
	 * @return - Boolean representing if the category parameter is found inside of the referenced list parameter
	 */
	public static boolean determineValidCategory(String category, List<String> list, int listSize) {
		for (int i = 0; i < listSize; i++) {
			if (list.get(i).equals(category)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param line- String that contains the Category + Sub Category
	 * @return - String that contains the Category of the line paramter
	 */
	public static String getCategory(String line) {
		return line.substring(0, line.indexOf(" "));
	}

	/**
	 * @param subCategory -  String referencing the incoming subCategory to be tests
	 * @return - Boolean representing if the subcategory has a valid format
	 */
	public static boolean determineValidSubCategory( String subCategory) {
		return subCategory.length() > 0;
	}

	/**
	 * 
	 * @param line - String that contains the Category + Sub Category
	 * @return - String of the subCategory of the input line parameter
	 */
	public static String getSubCategory(String line) {
		return line.substring(line.indexOf(" ") + 1).trim();

	}

}