package test.com.neustar.homework;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.Assert;


import com.neustar.homework.Main;

public class MainTest {
	
	
	
	@Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();
	
	/**
	 * Returns the valid file path for the default input file for Main.main()
	 */
	private String getValidFilePath(){
		URI validFilePath = null;
		try{	
			validFilePath= this.getClass().getResource("/resources/categories.txt").toURI();
		} catch(Exception e){
			e.printStackTrace();
		}
		return validFilePath.toString().substring("file:".length());
	}
	
	@Before
	public void before(){}
	
	
	/**
	 * Test for parameter not being provided
	 */
	@Test
    public void testNotEnoughArgsStatus() {
		exit.expectSystemExitWithStatus(1);
		Main.main(new String[] {});
    }
	
	/**
	 * Test for non-existent file on the host 
	 */
	@Test
	public void testNoneExistentFileStatus(){
		exit.expectSystemExitWithStatus(2);
		Main.main(new String[] {getValidFilePath()+"nonExistent"});
	}
	
	/**
	 * Test for successful run without any System.exit() calls
	 */
	@Test
	public void testSuccessfullRun(){
		getValidFilePath();
		exit.none();
		Main.main(new String[] {getValidFilePath()});
	}
	
	
	/**
	 * Test for Main.addLineToSavedList()
	 */
	@Test
	public void testAddLineToSavedList(){
		List<String> savedCategoriesList = new ArrayList<String>();
		Main.addLineToSavedList("OTHER TREE", savedCategoriesList);
		Main.addLineToSavedList("OTHER MOUSE", savedCategoriesList);
		Assert.assertEquals("List should have length of 2", 2, savedCategoriesList.size());
	}
	
	/**
	 * Tests for Main.addLineToSavedMap
	 */
	@Test
	public void testAddLineToSavedMap(){
		Map<String, List<String>> savedCategoriesMap = new HashMap<String, List<String>>();
		String category = "OTHER";
		Main.addLineToSavedMap(category, "TREE", savedCategoriesMap);
		Assert.assertEquals("List should have length of 1", 1, savedCategoriesMap.get(category).size());
		Main.addLineToSavedMap(category, "ROSE", savedCategoriesMap);
		Assert.assertEquals("List should have length of 2", 2, savedCategoriesMap.get(category).size());
	}
		
	/**
	 * Tests for Main.determineIfSubCategoryExistsInSavedCategories
	 */
	@Test
	public void testDetermineIfSubCategoryExistsInSavedCategories(){
		Map<String, List<String>> savedCategoriesMap = new HashMap<String, List<String>>();
		Main.addLineToSavedMap("OTHER", "TREE", savedCategoriesMap);
		Assert.assertEquals("This should return false", false, Main.determineIfSubCategoryExistsInSavedCategories("OTHER", "BOOK", savedCategoriesMap));
		Assert.assertEquals("This should return true", true, Main.determineIfSubCategoryExistsInSavedCategories("OTHER", "TREE", savedCategoriesMap));
	}
	
	/**
	 * Test for Main.getSubCategoryListInSavedCategories
	 */
	@Test
	public void testGetSubCategoryListInSavedCategories(){
		Map<String, List<String>> savedCategoriesMap = new HashMap<String, List<String>>();
		Main.addLineToSavedMap("OTHER", "TREE", savedCategoriesMap);
		Main.addLineToSavedMap("OTHER", "ROCK", savedCategoriesMap);
		Main.addLineToSavedMap("OTHER", "AIR", savedCategoriesMap);
		List<String> returnedList = Main.getSubCategoryListInSavedCategories("OTHER", savedCategoriesMap);
		Assert.assertEquals("Returned List size should be 3", 3, returnedList.size());
	}
	
	/**
	 * Tests for Main.determineValidLine()
	 */
	@Test
	public void determineValidLine(){
		String category = "OTHER";
		String subCategory = "Tree";
		Assert.assertEquals("Expected: " + true, true , Main.determineValidLine(category, subCategory,  Main.acceptedCategories, Main.acceptedCategoriesSize));
		category = "ANIMAL";
		Assert.assertEquals("Expected: " + false, false, Main.determineValidLine(category, "",  Main.acceptedCategories, Main.acceptedCategoriesSize));
	}
	
	
	/**
	 *  Test for Main.getCategory()
	 */
	@Test
	public void testGetCategory(){
		String category = "OTHER";
		Assert.assertEquals("Expected: " + category, category, Main.getCategory(category + " Tree"));
	}
	
	/**
	 * Tests for Main.determineValidCategory()
	 */
	@Test
	public void testDetermineValidCategory(){
		String category = "OTHER";
		Assert.assertEquals("Expected: true", true, Main.determineValidCategory(category, Main.acceptedCategories, Main.acceptedCategoriesSize));
		category = "PLANE";
		Assert.assertEquals("Expected: false", false, Main.determineValidCategory(category, Main.acceptedCategories, Main.acceptedCategoriesSize));
	}
	

	
	/**
	 * Tests for Main.determineValidSubCategory()
	 */
	@Test
	public void testDetermineValidSubCategory(){
		String subCategory = "Valid";
		boolean returnedSubCategory = Main.determineValidSubCategory(subCategory);
		Assert.assertEquals("Expecting Pass", true, returnedSubCategory );
		subCategory = "";
		returnedSubCategory = Main.determineValidSubCategory(subCategory);
		Assert.assertEquals("Expecting failure", false, returnedSubCategory );
	}
	
	/**
	 * Tests for Main.getSubCategory()
	 */
	@Test
	public void testGetSubCategory(){
		String subCategory = "PLANE";
		String returnedSubCategory = Main.getSubCategory("OTHER " + subCategory);
		Assert.assertEquals("Expecting PLANE", subCategory, returnedSubCategory );
		subCategory = "";
		returnedSubCategory = Main.getSubCategory("OTHER " + subCategory);
		Assert.assertEquals("Expecting ''", subCategory, returnedSubCategory );
	}
	
	
}
